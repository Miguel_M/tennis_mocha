var TennisGame2 = function(player1Name, player2Name) {
    this.P1point = 0;
    this.P2point = 0;

    this.P1res = "";
    this.P2res = "";

    this.player1Name = player1Name;
    this.player2Name = player2Name;
};

function isDeuce(P1point, P2point){
    if (P1point ===P2point && P1point > 2)
        return true;
    else
        return false;
}

function is30_40_15Love(P1point,P2point){
    if(P1point>0 && P2point===0)
        return true;
    else
        return false;
}
function isFifteen(P1point){
    if (P1point === 1)
        return true;
    else
        return false;
}
function isThirty(P1point){
    if (P1point === 2)
        return true;
    else
        return false;
}
function isForty(P1point){
    if (P1point === 3)
        return true;
    else
        return false;
}
function generateNumber(P1point){
    let score;
    if (isFifteen( P1point))
        score = "Fifteen";
    if (isThirty(P1point))
        score = "Thirty";
    if (isForty(P1point))
        score = "Forty";
    if (isLove(P1point))
         score = "Love";
    return score;
}
function isLove_15_30All(P1point,P2point){
    if(P1point === P2point && P1point < 3)
        return true;
    else
        return false;
}
function isLove(P1point){
    if (P1point === 0)
        return true;
    else
        return false;
}
function isLove30_40_15(P1point,P2point){
    if(P2point > 0 && P1point === 0)
        return true;
    else
        return false;
}

function onePointisGreaterThanOtherPointAndGreaterThan4(P1point,P2point){
    if(P1point >P2point &&P1point < 4)
        return true;
    else
        return false;
}

function isAdvantageFirstPlayer(P1point,P2point) {
    if (P1point > P2point && P2point >= 3)
        return true;
    else
        return false;
}

function winFirstPlayer(P1point,P2point){
    if(P1point >= 4 && P2point >= 0 && (P1point -P2point) >= 2)
        return true;
    else
        return false;
}


TennisGame2.prototype.getScore = function() {
    var score = "";

    if (isLove_15_30All(this.P1point,this.P2point)) {
       score =generateNumber(this.P1point);
       score += "-All";
    }
    if (isDeuce(this.P1point, this.P2point))
        score = "Deuce";

    if (is30_40_15Love(this.P1point, this.P2point)) {
        this.P1res = generateNumber(this.P1point);
        score = this.P1res + "-Love";
    }
    if (isLove30_40_15( this.P1point, this.P2point)) {
        this.P2res=generateNumber(this.P2point);
        score = "Love-" + this.P2res;
    }

    if (onePointisGreaterThanOtherPointAndGreaterThan4(this.P1point,this.P2point)) {
         this.P1res = generateNumber(this.P1point);
         this.P2res = generateNumber(this.P2point);
         score = this.P1res + "-" + this.P2res;
    }
    if (onePointisGreaterThanOtherPointAndGreaterThan4(this.P2point,this.P1point)) {
        this.P1res = generateNumber(this.P1point);
        this.P2res = generateNumber(this.P2point);
        score = this.P1res + "-" + this.P2res;
   }
    

    if (isAdvantageFirstPlayer(this.P1point, this.P2point)) {
        score = "Advantage player1";
    }

    if (isAdvantageFirstPlayer(this.P2point, this.P1point)) {
        score = "Advantage player2";
    }

    if (winFirstPlayer(this.P1point, this.P2point)) {
        score = "Win for player1";
    }
    if (winFirstPlayer(this.P2point, this.P1point)) {
        score = "Win for player2";
    }
    return score;
};

TennisGame2.prototype.SetP1Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P1Score();
    }
};

TennisGame2.prototype.SetP2Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P2Score();
    }
};

TennisGame2.prototype.P1Score = function() {
    this.P1point++;
};

TennisGame2.prototype.P2Score = function() {
    this.P2point++;
};

TennisGame2.prototype.wonPoint = function(player) {
    if (player === "player1")
        this.P1Score();
    else
        this.P2Score();
};

if (typeof window === "undefined") {
    module.exports = TennisGame2;
}
